+++
title = "Messaging Protocol"
+++

I always debate myself, how hostile messaging protocol right now, and do some research is answer for that.

<!-- more -->

And Hi, I trully on good condition when tying this article and only hit ~15 minutes to draft it. 

I finally come point to decrapted telegram and using XMPP as message protocol, I aleardy have good step on it and most people I contacted frequently aleardy migrated. I tested more like Matrix<a id="refnote1" href="#note1"><strong> ¹ </strong></a>/Rocket.chat/Session. But XMPP so far have more stable functionality than other one.

## Whatsapp and caveats

Its long journey since many years I refuse using any other message protocol, Especially on where country I live was every subject using Whatsapp in finance, bank, even internal government which strange for me. Because for time I was tried using it there something "broke" on Whatsapp which make me step back to using it.

**Notification**

I using phone without google services which making notification sometimes miss and require me to open app back<a id="refnote2" href="#note2"><strong> ² </strong></a>, I always make sure app was not closed by app manger android, but sadly notification still miss hit. On Telegram there "Background connection" make notification running smoothly without any hassle which I miss on Whatsapp.

**Application**

For my case, on linux there no desktop client so I use Web Application was install via Chromium, good thing is Whatsapp recently implemented sync messages without require phone actually running 24 hours which nice touch, but any registration still require android/ios before you can use Whatsapp which kinda "why", but hope this changes for future.

For App (web) was pretty bad, container was centered and many space was wasted on corner which not good. Also attachment broke when I tried upload image, there some "compress" algorithm which make image drop quality which not good either.

Also only 1 phones can be using 1 Whatsapp account, which deal-breaker for me because I have 2 phones which just in-case I require charging phone, I can bring other one (and no, I dont think bring powerbank is consider solution).

**Client not open-source**

I dont ask about ethics, its more like how you trust the app your using not sending unless what you want sending. In perfect world control what your sending is more important than fight with your "meta"-data aleardy send somewhere.

So Client should be open-source, its same approach what Telegram or Signal doing now, which you can audit the code and modified as your wish.

## XMPP

XMPP is open communication protocol, it means server only need follow protocol was written and everyone can make own implementation back-end without depends on specific app server. Unlike other message protocol I mentioned before. XMPP is federated which mean server was moderated by yourself and also you can invite everybody to join your server.

I using XMPP almost hit 2 years, I hit my head when trying self-hosting on first me, after some days trying figure out and fix things. I consider this protocol was good "enough"

**Notification**

And yes, notification wasn't fast than Telegram and more faster than Whatsapp. Its middle-way, I tested it on My Local VPS with 20ms ping to my ISP, notification running smoothly on my phone and nothing notification missing, which great.

**Application**

XMPP client wasn't good, non-techie people probably can be lost easily, and implemented hit and miss all around. There many client doesnt compilance with standard XEP which kinda sad because protocol was robust enough. Also Registration can be done via CLI/Web API which kinda nice without any PII<a id="refnote3" href="#note3"><strong> ³ </strong></a>. Message was sync if you OMEMO Properly.
For increase odd chance people success, here XMPP Client I recommended to use
- Android : Conversations (https://conversations.im)
- Linux : Dino (https://dino.im)
- Web : converse.js (https://conversejs.org)
- Windows : (coming soon)
- iOS : (coming soon)

Also 1 account can be used on multiple device, which nice.

I still using telegram but in pretty low volume, so for urgently, please use more proper channel like Mail / XMPP.

See ya~

<hr>

<ol>
<li id="note1">Matrix was okay and robust, but for some reason how they implement protocol wasn't not good IMO, its my personal bias.<a href="#refnote1">↩︎</a></li>
<li id="note2">I aware Whatsapp using GCM by Google Play Services, and android itself have weird app management. Telegram can accomplish it in easy-way and also code was open-source.<a href="#refnote2">↩︎</a></li>
<li id="note3">Unless you self-hosted you server, Some XMPP Server was require email / other information to recovery your account.<a href="#refnote3">↩︎</a></li>
<ol>
